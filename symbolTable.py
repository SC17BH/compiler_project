from compilerexceptions import BadIdentifierException
import sys

class SymbolTable:
    def __init__(self,tableName):
        # List for storing Symbols as a list with the format: [name,kind,type,offset,assigned]
        self.vars = []
        self.methods = []
        self.varCount = 0
        self.methodCount = 0
        self.tableName = tableName
        if len(sys.argv) > 1 and sys.argv[1] == "true":
            print("---" + self.tableName + "---")

    # Add a variable to the symbol table
    def addVar(self,name,kind,type,assigned = False):
        offset = 0
        for s in self.vars:
            if s[1] == kind:
                offset += 1
            if s[0] == name and s[1] == kind:
                return False
        self.vars.append([name,kind,type,offset,assigned])
        self.varCount += 1

    # Add a subroutine to the symbol table
    def addMethod(self,name,kind,type,assigned = False):
        offset = 0
        for s in self.methods:
            if s[1] == kind:
                offset += 1
            if s[0] == name and s[1] == kind:
                return False
        self.methods.append([name,kind,type,offset,assigned])
        self.methodCount += 1
    
    # Get a method from the symbol table, false if it not present
    def getVar(self,name):
        for v in self.vars:
            if v[0] == name:
                return v

        return False

    # Get a subroutine from the symbol table, false if it not present
    def getMethod(self,name):
        for m in self.methods:
            if m[0] == name:
                return m

        return False

    # Set a variable in the symbol table to having been assinged, false if no variable is present
    def setAssigned(self,name):
        v = self.getVar(name)
        if v:
            v[4] = True
            return True
        return False
        
    # Return true if the variable 'name' has been assinged to
    def isAssigned(self,name):
        v = self.getVar(name)
        if v and v[4] == True:
            return True
        return False

    # Print the table to the console (debugging)
    def printTable(self):
        print("---" + self.tableName + "---")
        for s in self.vars:
            print(str(s[0]) + " " + str(s[1]) + " " + str(s[2]) + " " + str(s[3]) + " " + str(s[4]))
        for s in self.methods:
            print(str(s[0]) + " " + str(s[1]) + " " + str(s[2]) + " " + str(s[3]) + " " + str(s[4]))
        print("---/"+ self.tableName+" ---")